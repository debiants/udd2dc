#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# udd2dc
# Ultimate Debian Database (UDD) data mining for Debian Contributors (DC)
#
# Copyright (C) 2014  Simó Albert i Beltran <sim6@probeta.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import json
from debiancontributors.types import Identifier
import psycopg2
import requests

if len(sys.argv) < 2:
	print("""
Usage: %s <token> [<last_update_date>]
""" % sys.argv[0] )
	exit(1)

token = sys.argv[1]

if len(sys.argv) == 3:
	last_update_date = sys.argv[2]
else:
	last_update_date = "-infinity"

dc_api_url = "https://contributors.debian.org/contributors/post"

conn = psycopg2.connect("service=udd")
cursor = conn.cursor()
cursor.execute("""
select
	submitter_email,
	min(arrival),
	max(arrival)
from
	all_bugs
where
	arrival != 'epoch'
	and
	arrival <= 'now'
group by
	submitter_email
having
	count(*) > 1
	and
	max(arrival) >= '%s'
""" % last_update_date)
data = []
for item in cursor.fetchall():

    email = item[0]
    if not Identifier.TYPE_VALIDATORS["email"].match(email):
        continue
    begin = str(item[1].date())
    end = str(item[2].date())

    id = dict()
    id["type"] = "email"
    id["id"] = email

    ids = [ id ]

    contribution = dict()
    contribution["type"] = "bug-submission"
    contribution["begin"] = begin
    contribution["end"] = end
    contribution["url"] = "https://bugs.debian.org/cgi-bin/pkgreport.cgi?correspondent=" + email

    contributions = [ contribution ]

    contributor = dict()
    contributor["id"] = ids
    contributor["contributions"] = contributions

    data.append(contributor)

payload = {}
payload['auth_token'] = token
payload['source'] = "bugs.debian.org"
files = { 'data': json.dumps(data) }

r = requests.post(dc_api_url, data=payload, files=files)

print(r.text)

if r.status_code != 200:
	exit(1)
